package dev.ariles.mvidemo.android.chucksfacts

import androidx.lifecycle.LiveData

interface IViewModel<STATE, INTENT> {

    val state: LiveData<STATE>

    fun dispatchIntent(intent: INTENT)
}