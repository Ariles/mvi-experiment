package dev.ariles.mvidemo.android.chucksfacts

interface IViewRenderer<STATE> {
    fun render(state: STATE)
}