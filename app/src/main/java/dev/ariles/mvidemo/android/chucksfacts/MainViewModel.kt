package dev.ariles.mvidemo.android.chucksfacts

import androidx.annotation.UiThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dev.ariles.mvidemo.data.repositories.ChuckNorrisRepository
import dev.ariles.mvidemo.data.api.IChuckNorrisService
import dev.ariles.mvidemo.domain.entities.Fact
import dev.ariles.mvidemo.domain.entities.FactsCategory
import dev.ariles.mvidemo.domain.exceptions.GenericNetworkException
import dev.ariles.mvidemo.domain.usecases.ChuckNorrisFactsUseCase
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlin.coroutines.CoroutineContext

class NonNullMutableLiveData<T> @UiThread constructor(initialValue: T) : MutableLiveData<T>() {

    init {
        value = initialValue
    }

    override fun getValue(): T {
        return super.getValue()!!
    }
}

class MainViewModel : ViewModel(),
    IViewModel<State, UserIntent>, CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Job() + Dispatchers.Default

    private val stateChannel = Channel<PartialState>()

    private val _state =
        NonNullMutableLiveData(createInitialState())
    override val state: LiveData<State>
        get() = _state

    private val chuckNorrisUseCase = ChuckNorrisFactsUseCase(
        ChuckNorrisRepository(IChuckNorrisService.newInstance())
    )
    private val reducer: IReducer<State, PartialState> =
        Reducer()

    init {
        launch(Dispatchers.Main) {
            for (partialState in stateChannel) {
                _state.value = reducer.reduce(_state.value, partialState)
            }
        }
        launch {
            handleAction(Action.FetchCategories)
        }
    }

    private fun createInitialState() = State(
        isLoadingCategories = false,
        isLoadingFact = false,
        facts = emptyList(),
        categories = emptyList(),
        isKickButtonEnabled = false,
        isClearButtonEnabled = false,
        event = null
    )

    override fun dispatchIntent(intent: UserIntent) {
        handleAction(intentToAction(intent))
    }

    private fun intentToAction(intent: UserIntent): Action {
        return when (intent) {
            is UserIntent.ShowNewFact -> Action.FetchRandomFact(
                intent.category
            )
            UserIntent.ClearFact -> Action.ClearFact
        }
    }

    private fun handleAction(action: Action) {
        launch {
            return@launch when (action) {

                is Action.FetchRandomFact -> {

                    stateChannel.send(
                        PartialState.Loading(
                            isLoadingCategories = false,
                            isLoadingFact = true
                        )
                    )

                    delay(1000)

                    chuckNorrisUseCase.getRandom(action.category).either({ joke ->
                        stateChannel.send(
                            PartialState.JokeRetrievedSuccessfully(
                                joke
                            )
                        )
                    }, { exception ->
                        stateChannel.send(
                            PartialState.FetchJokeFailed(
                                exception
                            )
                        )
                    })
                }

                Action.FetchCategories -> {

                    stateChannel.send(
                        PartialState.Loading(
                            isLoadingCategories = true,
                            isLoadingFact = false
                        )
                    )

                    delay(1200)

                    chuckNorrisUseCase.getCategories().either({ categories ->
                        stateChannel.send(
                            PartialState.CategoriesRetrievedSuccessfully(
                                categories
                            )
                        )
                    }, { exception ->
                        stateChannel.send(
                            PartialState.FetchJokeFailed(
                                exception
                            )
                        )
                    })
                }

                Action.ClearFact -> {
                    stateChannel.send(PartialState.JokesCleared)
                }
            }
        }
    }
}

sealed class PartialState {

    object JokesCleared : PartialState() {
        val joke = emptyList<Fact>()
        const val isJokeButtonEnabled = true
        const val isClearButtonEnabled = false
    }

    data class Loading(val isLoadingFact: Boolean, val isLoadingCategories: Boolean) :
        PartialState() {
        val isJokeButtonEnabled = false
    }

    data class JokeRetrievedSuccessfully(val fact: Fact) : PartialState() {
        val isJokeButtonEnabled = true
        val isClearButtonEnabled = true
        val isLoadingFact = false
    }

    data class CategoriesRetrievedSuccessfully(val categories: List<FactsCategory>) :
        PartialState() {
        val isJokeButtonEnabled = true
        val isClearButtonEnabled = false
        val isLoadingCategories = false
    }

    data class FetchJokeFailed(val exception: GenericNetworkException) : PartialState() {
        val isJokeButtonEnabled = true
        val isLoadingFact = false
        val isLoadingCategories = false
    }

    data class FetchCategoriesFailed(val exception: GenericNetworkException) : PartialState() {
        val isLoadingCategories = false
    }
}

private sealed class Action {
    data class FetchRandomFact(val category: String?) : Action()
    object ClearFact : Action()
    object FetchCategories : Action()
}