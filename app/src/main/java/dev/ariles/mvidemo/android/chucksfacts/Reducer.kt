package dev.ariles.mvidemo.android.chucksfacts

import android.widget.Toast

interface IReducer<STATE, PARTIAL_STATE> {
    fun reduce(state: STATE, partialState: PARTIAL_STATE): STATE
}

class Reducer : IReducer<State, PartialState> {

    override fun reduce(state: State, partialState: PartialState): State {
        return when (partialState) {

            is PartialState.JokesCleared -> state.copy(
                isClearButtonEnabled = PartialState.JokesCleared.isClearButtonEnabled,
                isKickButtonEnabled = PartialState.JokesCleared.isJokeButtonEnabled,
                facts = PartialState.JokesCleared.joke
            )

            is PartialState.JokeRetrievedSuccessfully -> state.copy(
                isClearButtonEnabled = partialState.isClearButtonEnabled,
                isKickButtonEnabled = partialState.isJokeButtonEnabled,
                isLoadingFact = partialState.isLoadingFact,
                facts = state.facts.toMutableList().apply { add(0, partialState.fact) }
            )

            is PartialState.CategoriesRetrievedSuccessfully -> state.copy(
                categories = partialState.categories.map { it.title },
                isClearButtonEnabled = partialState.isClearButtonEnabled,
                isKickButtonEnabled = partialState.isJokeButtonEnabled,
                isLoadingCategories = partialState.isLoadingCategories
            )

            is PartialState.Loading -> state.copy(
                isKickButtonEnabled = partialState.isJokeButtonEnabled,
                isLoadingFact = partialState.isLoadingFact,
                isLoadingCategories = partialState.isLoadingCategories
            )

            is PartialState.FetchJokeFailed -> {
                state.copy(
                    isKickButtonEnabled = partialState.isJokeButtonEnabled,
                    isLoadingFact = partialState.isLoadingFact,
                    isLoadingCategories = partialState.isLoadingCategories,
                    event = { context ->
                        Toast.makeText(
                            context,
                            partialState.exception.javaClass.simpleName,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                )
            }

            is PartialState.FetchCategoriesFailed -> {
                state.copy(
                    isLoadingCategories = partialState.isLoadingCategories,
                    event = { context ->
                        Toast.makeText(
                            context,
                            partialState.exception.javaClass.simpleName,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                )
            }
        }
    }
}