package dev.ariles.mvidemo.android.chucksfacts

import android.content.Context
import dev.ariles.mvidemo.domain.entities.Fact

data class State(
    val isLoadingCategories: Boolean,
    val isLoadingFact: Boolean,
    val facts: List<Fact>,
    val categories: List<String>,
    val isKickButtonEnabled: Boolean,
    val isClearButtonEnabled: Boolean,
    val event: ((Context) -> Unit)?
)