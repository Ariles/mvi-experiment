package dev.ariles.mvidemo.android.chucksfacts

sealed class UserIntent {
    data class ShowNewFact(val category: String?) : UserIntent()
    object ClearFact : UserIntent()
}