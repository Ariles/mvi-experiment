package dev.ariles.mvidemo.data.repositories

import com.squareup.moshi.JsonReader
import dev.ariles.mvidemo.data.api.IChuckNorrisService
import dev.ariles.mvidemo.domain.abstractions.IChuckNorrisRepository
import dev.ariles.mvidemo.domain.entities.Fact
import dev.ariles.mvidemo.domain.entities.FactsCategory
import dev.ariles.mvidemo.domain.exceptions.GenericNetworkException
import org.json.JSONException
import java.io.IOException

class ChuckNorrisRepository(private val api: IChuckNorrisService) : IChuckNorrisRepository {

    @Throws(GenericNetworkException::class)
    override suspend fun getRandomFact(category: String?): Fact {
        try {
            val call  = if(category != null){
                api.getRandomJoke(category)
            }else {
                api.getRandomJoke()
            }
            println(call.request().url())
            call.execute().body()?.apply {
                return this
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        throw GenericNetworkException()
    }

    @Throws(GenericNetworkException::class)
    override suspend fun getCategories(): List<FactsCategory> {
        try {
            api.getCategories().execute()
                .body()?.source()
                ?.apply {
                    val reader = JsonReader.of(this)
                    return (reader.readJsonValue() as List<String>)
                        .map { FactsCategory(it) }

                }

        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        throw GenericNetworkException()
    }
}