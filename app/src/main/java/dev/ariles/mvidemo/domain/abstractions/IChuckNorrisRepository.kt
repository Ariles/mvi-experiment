package dev.ariles.mvidemo.domain.abstractions

import dev.ariles.mvidemo.domain.entities.Fact
import dev.ariles.mvidemo.domain.entities.FactsCategory
import dev.ariles.mvidemo.domain.exceptions.GenericNetworkException

interface IChuckNorrisRepository {

    @Throws(GenericNetworkException::class)
    suspend fun getRandomFact(category: String? = null): Fact

    @Throws(GenericNetworkException::class)
    suspend fun getCategories(): List<FactsCategory>
}