package dev.ariles.mvidemo.domain.entities

data class Fact(
    val value: String,
    val icon_url: String
)