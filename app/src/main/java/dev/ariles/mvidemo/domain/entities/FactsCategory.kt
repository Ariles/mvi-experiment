package dev.ariles.mvidemo.domain.entities

data class FactsCategory(
    val title: String
)