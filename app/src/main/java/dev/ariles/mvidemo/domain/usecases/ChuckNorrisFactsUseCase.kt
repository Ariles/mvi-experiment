package dev.ariles.mvidemo.domain.usecases

import dev.ariles.mvidemo.domain.utils.Either
import dev.ariles.mvidemo.domain.abstractions.IChuckNorrisRepository
import dev.ariles.mvidemo.domain.entities.Fact
import dev.ariles.mvidemo.domain.entities.FactsCategory
import dev.ariles.mvidemo.domain.exceptions.GenericNetworkException

class ChuckNorrisFactsUseCase(private val repository: IChuckNorrisRepository) {

    suspend fun getRandom(category: String? = null): Either<Fact, GenericNetworkException> {
        return try {
            Either.Success(repository.getRandomFact(category))
        } catch (exception: GenericNetworkException) {
            Either.Failure(exception)
        }
    }

    suspend fun getCategories(): Either<List<FactsCategory>, GenericNetworkException> {
        return try {
            Either.Success(repository.getCategories())
        } catch (exception: GenericNetworkException) {
            Either.Failure(exception)
        }
    }
}