package dev.ariles.mvidemo.android.listbooks

import dev.ariles.mvidemo.android.chucksfacts.PartialState
import dev.ariles.mvidemo.android.chucksfacts.Reducer
import dev.ariles.mvidemo.android.chucksfacts.State
import dev.ariles.mvidemo.domain.entities.Fact
import org.hamcrest.CoreMatchers.`is`
import org.junit.Test

import org.junit.Assert.*

class ReducerTest {

    @Test
    fun `reduce JokeRetrievedSuccessfully should add a joke to the top of the list`() {
        //Given
        val someFact = Fact("some fact title", "https://fake.com/some-fact-url.png")
        val newFact = Fact("new fact title","https://fake.com/new-fact-url.png")

        val currentState = State(
            isLoadingCategories = false,
            isLoadingFact = true,
            facts = listOf(someFact),
            categories = emptyList(),
            isKickButtonEnabled = false,
            isClearButtonEnabled = false,
            event = null
        )

        val partialState = PartialState.JokeRetrievedSuccessfully(
            newFact
        )

        val expectedNewState = currentState.copy(
            facts = listOf(newFact) + currentState.facts,
            isLoadingFact = false,
            isKickButtonEnabled = true,
            isClearButtonEnabled = true
        )

        val reducer = Reducer()

        //When
        val newState = reducer.reduce(currentState, partialState)

        //Then
        assertThat(newState, `is`(expectedNewState))
    }
}